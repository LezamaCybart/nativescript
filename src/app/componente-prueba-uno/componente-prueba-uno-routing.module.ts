import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { ComponentePruebaUnoComponent } from "./componente-prueba-uno.component"

const routes: Routes = [
    { path: "", component: ComponentePruebaUnoComponent }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class ComponentePruebaUnoRoutingModule { }
