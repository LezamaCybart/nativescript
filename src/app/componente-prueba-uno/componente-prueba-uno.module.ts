import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { ComponentePruebaUnoRoutingModule } from "./componente-prueba-uno-routing.module";
import { ComponentePruebaUnoComponent } from "./componente-prueba-uno.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        ComponentePruebaUnoRoutingModule
    ],
    declarations: [
        ComponentePruebaUnoComponent
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class ComponentePruebaUnoModule { }
