import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/page";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toast"
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.mode";
import { Store } from "@ngrx/store";
import { registerElement } from "@nativescript/angular"
import { AppState } from "../app.module";
import * as SocialShare from "nativescript-social-share";

registerElement("PullToRefresh", () => require("@nstudio/nativescript-pulltorefresh").PullToRefresh);


@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    providers: [NoticiasService],
})
export class SearchComponent implements OnInit {
    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(
        public noticias: NoticiasService,
        public store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }



    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(x): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    refreshList(args) {
        const pullRefresh = args.object;
        setTimeout(function () {
           pullRefresh.refreshing = false;
        }, 1000);
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string) {
        console.dir("BuscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados BuscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error BuscarAhora: " + e);
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        });
        
}
