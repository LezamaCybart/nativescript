import { Component, OnInit } from "@angular/core";
import { RadSideDrawer, ScaleDownPusherTransition } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as Toast from "nativescript-toasts"

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 1000); }

    ngOnInit(): void {
        this.doLater(() =>
            dialogs.action("Mensaje", "cancelar!", ["Opcion 1", "Opcion 2"])
                .then((result) => {
                    console.log("resultado: " + result);
                    if (result === "Opcion 1") {
                        this.doLater(() =>
                            dialogs.alert({
                                title: "Titulo 1",
                                message: "mje1",
                                okButtonText: "btn 1"
                            }).then(() => console.log("cerrado1")));
                    }else if (result === "Opcion 2") {
                        this.doLater(() =>
                            dialogs.alert({
                                title: "Titulo 1",
                                message: "mje1",
                                okButtonText: "btn 1"
                            }).then(() => console.log("cerrado1")));

                    }
                    
                }
        ));

        const toastOptions: Toast.ToastOptions = {text: "Hello World", duration: Toast.DURATION.SHORT};
        this.doLater(() => Toast.show(toastOptions));

    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
